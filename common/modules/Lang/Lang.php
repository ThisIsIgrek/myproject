<?php

namespace common\modules\Lang;

use phpDocumentor\Reflection\Types\Integer;
use Yii;

/**
 * This is the model class for table "{{%lang}}".
 *
 * @property int $id
 * @property string $url
 * @property string $local
 * @property string $name
 * @property int $default
 * @property int $date_update
 * @property int $date_create
 *
 * @property ProductLang[] $productLangs
 */
class Lang extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%lang}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['url', 'local', 'name', 'date_update', 'date_create'], 'required'],
            [['default', 'date_update', 'date_create'], 'integer'],
            [['url', 'local', 'name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'url' => Yii::t('app', 'Url'),
            'local' => Yii::t('app', 'Local'),
            'name' => Yii::t('app', 'Name'),
            'default' => Yii::t('app', 'Default'),
            'date_update' => Yii::t('app', 'Date Update'),
            'date_create' => Yii::t('app', 'Date Create'),
        ];
    }

    /**
     * Gets query for [[ProductLangs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductLangs()
    {
        return $this->hasMany(ProductLang::className(), ['lang_id' => 'id']);
    }

    public static function getIdLang($url=null)
    {
        if ($url == null) $url = self::getDefaultLang();
        return self::find()->where(['url' => $url])->one()->id;
    }

    //Переменная, для хранения текущего объекта языка
    static $current = null;

    //Получение текущего объекта языка
    static function getCurrent()
    {
        if( self::$current === null ){
            self::$current = self::getDefaultLang();
        }
        return self::$current;
    }

    //Получения объекта языка по умолчанию
    static function getDefaultLang()
    {
        return self::find()->where('`default` = :default', [':default' => 1])->one();
    }
}
