<?php
namespace common\models;

use Yii;
use yii\db\ActiveRecord;

class Products extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%products}}';
    }
}
