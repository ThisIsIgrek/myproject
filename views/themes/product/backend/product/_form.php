<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use modules\product\models\Category;
use modules\product\models\ProductCategory;

/* @var $this yii\web\View */
/* @var $model modules\product\models\Product */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="product-form">


    <?php
//     dd($model->categories);
    ?>


    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'categories')->dropDownList(Category::getList(), [
        'class' => 'form-control select2',
        'multiple' => "multiple"
    ]) ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
