<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel modules\product\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    --><?//= GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'id',
//            'name',
//            [
//                'attribute' => 'categories',
//                'value' => function ($item) {
//                    $str = '';
//                    foreach ($item->categories as $category) {
//                        $str .= $category->name . ' | ';
//                    }
//                    return $str;
//                },
//            ],
//
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]); ?>

<div>
<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'id',
        [
            'label' => 'Привет',
            'format' => 'raw',
            'contentOptions' => ['class' => 'align-middle'],
            'headerOptions' => ['class' => 'align-middle'],
            'value' => function (\modules\product\models\Product $model) {
                return $model->content->name;
            },
        ],

        ['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>


</div>

<div>
    <p>Установленный язык в Yii::$app->language: <?=Yii::$app->language?></p>
</div>

<div>
    <?= Html::a('Change language', ['/product/product', 'language' => 'en']) ?>
    <?= Html::a('Сменить язык', ['/product/product', 'language' => 'ru']) ?>
</div>
