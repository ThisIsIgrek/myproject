<?php

namespace frontend\views\site;
/* @var $this yii\web\View */

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\WLang;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Установленный язык в Yii::$app->language: <?=Yii::$app->language?></p>
    <p>Строка перевода <br> <?=Yii::t('app', 'Это страница "О нас". Вы можете изменить следующий файл, чтобы настроить его содержимое')?></p>

    <br><br>
    <p>This is the About page. You may modify the following file to customize its content:</p>

    <code><?= __FILE__ ?></code>
</div>

<div>
    <?= Html::a('Change language', ['site/about', 'language' => 'en']) ?>
    <?= Html::a('Сменить язык', ['site/about', 'language' => 'ru']) ?>
</div>

<div>
    ss
<!--    --><?//= WLang::widget();?>

</div>