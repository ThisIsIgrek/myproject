<?php
use yii\helpers\Html;


$this->title = 'Product page';
?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Страница продукты</title>

</head>
<body>


    <div class="product-item">
        <ul>
        <?php foreach ($products as $product): ?>

                <h3><?=$product->name?></h3>
                <h5>В наличии: <?=$product->count?></h5>
                <span class="price"><?=$product->price?> грн.</span>
                <a href="" class="button">Купить</a>
        <?php endforeach; ?>
    </ul>
    </div>

</div>
</body>
</html>