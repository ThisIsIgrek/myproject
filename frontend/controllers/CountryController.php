<?php

namespace frontend\controllers;

use yii\web\Controller;
use yii\data\Pagination;
use app\models\Country;


class CountryController extends Controller
{

    public function vardump($var)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }

    public function actionIndex()
    {
        $query = Country::find();

        $pagination = new Pagination([
            'defaultPageSize' => 5,
            'totalCount' => $query->count(),
        ]);

        $countries = $query->orderBy('name')
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

//        dd('ikjij');

        return $this->render('index', [
            'countries' => $countries,
            'pagination' => $pagination,
        ]);
    }
}