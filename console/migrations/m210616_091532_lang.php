<?php

use yii\db\Migration;
use yii\db\Schema;
/**
 * Class m210616_091532_lang
 */
class m210616_091532_lang extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%lang}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'url' => Schema::TYPE_STRING . '(255) NOT NULL',
            'local' => Schema::TYPE_STRING . '(255) NOT NULL',
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'default' => Schema::TYPE_SMALLINT . ' NOT NULL DEFAULT 0',
            'date_update' => Schema::TYPE_INTEGER . ' NOT NULL',
            'date_create' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('lang', ['url', 'local', 'name', 'default', 'date_update', 'date_create'], [
            ['en', 'en-EN', 'English', 0, time(), time()],
            ['ru', 'ru-RU', 'Русский', 1, time(), time()],
        ]);

        $this->createTable('{{%product_lang}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'lang_id' => $this->integer()->unsigned()->notNull(),
            'product_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
        ], 'ENGINE InnoDB');

        $this->createIndex(
            'idx-product_lang-product_id',
            '{{%product_lang}}',
            'product_id'
        );

        $this->createIndex(
            'idx-product_lang-lang_id',
            '{{%product_lang}}',
            'lang_id'
        );

        $this->addForeignKey(
            'fk-product_lang-product_id',
            '{{%product_lang}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-product_lang-lang_id',
            '{{%product_lang}}',
            'lang_id',
            '{{%lang}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210616_091532_lang cannot be reverted.\n";

        $this->dropTable('{{%lang}}');

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210616_091532_lang cannot be reverted.\n";

        return false;
    }
    */
}
