<?php

use yii\db\Migration;

/**
 * Class m210614_133537_new_table_product
 */
class m210614_133537_new_table_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
        ], 'ENGINE InnoDB');

        $this->createTable('{{%category}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
        ], 'ENGINE InnoDB');

        $this->createTable('{{%product_category}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'category_id' => $this->integer()->unsigned()->notNull(),
            'product_id' => $this->integer()->unsigned()->notNull(),
        ], 'ENGINE InnoDB');

        $this->createIndex(
            'idx-product_category-category_id',
            '{{%product_category}}',
            'category_id'
        );

        $this->createIndex(
            'idx-product_category-product_id',
            '{{%product_category}}',
            'product_id'
        );

        $this->addForeignKey(
            'fk-product_category-category_id',
            '{{%product_category}}',
            'category_id',
            '{{%category}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-product_category-product_id',
            '{{%product_category}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->insert('{{%product}}', ['name' => 'Зеркало с подсветкой']);
        $this->insert('{{%product}}', ['name' => 'Зеркальное полотно']);
        $this->insert('{{%product}}', ['name' => 'Зеркальное панно']);
        $this->insert('{{%product}}', ['name' => 'Душевая кабина']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210614_133537_new_table_product cannot be reverted.\n";

        return false;
    }

}
