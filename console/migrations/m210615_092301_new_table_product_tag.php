<?php

use yii\db\Migration;

/**
 * Class m210615_092301_new_table_product_tag
 */
class m210615_092301_new_table_product_tag extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%tag}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'name' => $this->string(255)->notNull(),
        ], 'ENGINE InnoDB');

        $this->createTable('{{%product_tag}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'tag_id' => $this->integer()->unsigned()->notNull(),
            'product_id' => $this->integer()->unsigned()->notNull(),
        ], 'ENGINE InnoDB');

        $this->createIndex(
            'idx-product_tag-product_id',
            '{{%product_tag}}',
            'product_id'
        );

        $this->createIndex(
            'idx-product_tag-tag_id',
            '{{%product_tag}}',
            'tag_id'
        );

        $this->addForeignKey(
            'fk-product_tag-product_id',
            '{{%product_tag}}',
            'product_id',
            '{{%product}}',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-product_tag-tag_id',
            '{{%product_tag}}',
            'tag_id',
            '{{%tag}}',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m210615_092301_new_table_product_tag cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210615_092301_new_table_product_tag cannot be reverted.\n";

        return false;
    }
    */
}
