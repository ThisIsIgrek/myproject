<?php
namespace modules\product\services;


use modules\product\models\Product;
use modules\product\models\Category;
use modules\product\models\ProductCategory;
use yii\base\model;


class ProductService extends Model
{
    /** @var Product */
    private $product;

    private $categories;


    /**
     * OfferService constructor.
     * @param Product $product
     * @param array $config
     */

    public function __construct(Product $product, array $config = [])
    {
        parent::__construct($config);

        $this->product = $product;
    }

    public function load($data, $formName = null) : bool
    {
        parent::load($data, $formName);

        try {
            $newCategories = $data['Product']['categories'];

            $categories = ProductCategory::find()
                ->where(['product_id' => $this->product->id])
                ->all();

            foreach ($categories as $category) {
                $category->delete();
            }

            foreach ($newCategories as $newCategory) {
                $model = new ProductCategory();
                $model->product_id = $this->product->id;
                $model->category_id = $newCategory;
                $this->categories[] = $model;
            }

            return true;
        }
        catch (\Exception $exception) {
            return false;
       }
    }

    public function save() : bool
    {
        foreach ($this->categories as $category){
              $category->save();
        }

        return true;
    }

}