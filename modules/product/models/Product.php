<?php

namespace modules\product\models;


use common\modules\Lang\ProductLang;
use common\modules\Lang\Lang;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "{{%product}}".
 *
 * @property int $id
 * @property string $name
 *
 * @property ProductCategory[] $productCategories
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%product}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[ProductCategories]].
     *
     * @return \yii\db\ActiveQuery
     */
        public function getProductCategories()
        {
            return $this->hasMany(ProductCategory::className(), ['product_id' => 'id']);
        }

    public function getCategories()
    {
        return $this->hasMany(Category::class, ['id' => 'category_id'])
            ->viaTable('{{%product_category}}', ['product_id' => 'id']);
    }

    public function setCategories($newCategories = [])
    {
        try {
            $categories = ProductCategory::find()
                ->where(['product_id' => $this->id])
                ->all();

            foreach ($categories as $category) {
                $category->delete();
            }

            foreach ($newCategories as $newCategory) {
                $model = new ProductCategory();
                $model->product_id = $this->id;
                $model->category_id = $newCategory;
                $model->save();
            }

            return true;
        }
        catch (\Exception $exception) {
            return false;
        }
    }

    public function vardump($var) {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }

    public function getTags()
    {
        return $this->hasMany(Tag::class, ['id' => 'product_id'])
            ->viaTable('{{%product_tag}}', ['tag_id' => 'id']);
    }

    public function getCategoryDropdown()
    {
        $listCategory = Category::find()->select('ID,name')
            ->all();
        $list = ArrayHelper::map( $listCategory,'ID','name');

        return $list;
    }

    public static function onLanguageChanged($event)
    {
        // $event->language: new language
        // $event->oldLanguage: old language

        // Save the current language to user record
        $user = Yii::$app->user;
        if (!$user->isGuest) {
            $user->identity->language = $event->language;
            $user->identity->save();
        }
    }

    public function getContent($lang_id=null)
    {
        // получение ID языка из урла:
        $lang_id = ($lang_id === null) ? Lang::getIdLang(Yii::$app->language) : $lang_id;

        return $this->hasOne(ProductLang::class, ['product_id' => 'id'])->where(['lang_id' => $lang_id]);
    }

//    public function getProducts()
//    {
//        return $this->hasMany(OrderItem::class, ['product_id' => 'id']);
//    }
//
//    public function getCategory()
//    {
//        return $this->hasMany(Item::class, ['id' => 'category_id'])
//            ->via('productCategory');
//    }

}
